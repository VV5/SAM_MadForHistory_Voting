﻿using UnityEngine;
using TMPro;
using System.IO;

public class Player : MonoBehaviour
{
	public int Raffles_Votes = 0;
	public int Farquhar_Votes = 0;
	public int Vote_Difference = 0;
	int TotalVotes;
	float RMorphPercentage;
	ArduinoHandler AH;
	Animator anim;
	AudioManager AM;
	Animator UIAnim;
	Animator UIAnim_R;

	public TMP_Text Raffles_Vote_Text;
	public TMP_Text Farquhar_Vote_Text;

	public GameObject Raffles_Particles;
	public GameObject Farquhar_Particles;
	public GameObject origin_Raffles;
	public GameObject origin_Farquhar;
	public GameObject RafflesUI;
	public GameObject FarquharUI;

	private void Start()
	{
		
		UIAnim = GameObject.Find("Farquhar_Ticket").GetComponent<Animator>();
		UIAnim_R = GameObject.Find("Raffles_Ticket").GetComponent<Animator>();
		AH = GetComponent<ArduinoHandler>();
		AM = GameObject.FindObjectOfType<AudioManager>();
		anim = GameObject.Find("Statue").GetComponent<Animator>();
		anim.speed = 0.0f;
		
		string path = Application.persistentDataPath + "/Player.fun";
		if (File.Exists(path))
		{
			LoadPlayer();
			MorphCalculator();
		}

		else
		{
			Debug.Log("Saving first time");
			SavePlayer();
		}
		
	}

	public void ControlAnimationFrame(float jumpframe)
	{
		anim.Play("MorphAnimation", 0, jumpframe);
	}


	public void MorphCalculator()
	{
		TotalVotes = Farquhar_Votes + Raffles_Votes;
		// We use Raffles as reference element. Morphing Percentages start from 0% to 100%
		if (TotalVotes != 0)
		{
			RMorphPercentage = Farquhar_Votes / (float)TotalVotes;
		}
		Debug.Log(RMorphPercentage);
		ControlAnimationFrame(RMorphPercentage);
	}

	public void SavePlayer()
	{
		SaveSystem.SavePlayer(this);
	}

	public void LoadPlayer()
	{
		PlayerData data = SaveSystem.LoadPlayer();
		Raffles_Votes = data.Raffles_Votes;
		Farquhar_Votes = data.Farquhar_Votes;
	}
	public void IncrementVote_Raffles()
	{
		Raffles_Votes++;

		SavePlayer();
		AM.RafflesRandomAudio();
		UIAnim_R.SetTrigger("IsRaffles");

		StartRafflesParticles();
	}
	public void IncrementVote_Farquhar()
	{
		Farquhar_Votes++;
	
		SavePlayer();
		AM.FarquharRandomAudio();

		UIAnim.SetTrigger("IsFarquhar");
		StartFarquharParticles();
	}
	public void ResetData()
	{
		Raffles_Votes = Farquhar_Votes = Vote_Difference = 0;
	}
	public void Update()
	{
		Raffles_Vote_Text.SetText(Raffles_Votes.ToString());
		Farquhar_Vote_Text.SetText(Farquhar_Votes.ToString());
		Vote_Difference = Mathf.Abs (Raffles_Votes - Farquhar_Votes);

		if (Raffles_Votes == Farquhar_Votes || TotalVotes == 0 )
		{
			ControlAnimationFrame(0.5f);
		}

	}
	public void StartRafflesParticles()
	{
		if(Raffles_Particles != null)
		{
			var vfx_R = Instantiate(Raffles_Particles, origin_Raffles.transform.position,Quaternion.identity) as GameObject;
			
			vfx_R.transform.SetParent(RafflesUI.GetComponent<RectTransform>());
			vfx_R.transform.SetAsFirstSibling();
			var ps = vfx_R.GetComponent<ParticleSystem>();
			Destroy(vfx_R, ps.main.duration + ps.main.startLifetime.constantMax);
		}
	}
	public void StartFarquharParticles()
	{
		if (Farquhar_Particles != null)
		{
			var vfx_F = Instantiate(Farquhar_Particles, origin_Farquhar.transform.position, Quaternion.identity) as GameObject;

			vfx_F.transform.SetParent(FarquharUI.GetComponent<RectTransform>());
			vfx_F.transform.SetAsFirstSibling();
			var ps = vfx_F.GetComponent<ParticleSystem>();
			Destroy(vfx_F, ps.main.duration + ps.main.startLifetime.constantMax);
		}
	}
}
