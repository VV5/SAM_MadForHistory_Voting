﻿using UnityEngine;
using System.IO.Ports;


public class ArduinoHandler: MonoBehaviour
{
	public Player PL_;
	SerialPort stream = new SerialPort("COM6", 9600);
	public string value_;
	int buttonState = 0;
	void Start()
	{
		stream.Open();
		stream.ReadTimeout = 10;
		PL_ = GetComponent<Player>();
	}

	void FixedUpdate()
	{

		 value_= stream.ReadLine();
		try
		{
			print(value_);
		}
		catch (System.TimeoutException)
		{
		}
		if (value_ == "Vote_Raffles/n")
		{

			PL_. IncrementVote_Raffles();
			PL_.MorphCalculator();
		}
		if (value_ == "Vote_Farquhar/n")
		{
			PL_. IncrementVote_Farquhar();
			PL_.MorphCalculator();
	
		}
	}



}