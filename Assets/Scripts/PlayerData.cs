﻿
[System.Serializable]
public class PlayerData
{
	public int Raffles_Votes;
	public int Farquhar_Votes;

	public PlayerData(Player player)
	{
		Raffles_Votes = player.Raffles_Votes;
		Farquhar_Votes = player.Farquhar_Votes;
	}
}
