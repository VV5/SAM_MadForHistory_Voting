﻿
using UnityEngine;

public class Animation2D : MonoBehaviour
{
	Animator anim;
    // Start is called before the first frame update
    void Start()
    {
		anim = GetComponent<Animator>();
		anim.SetBool("Jump",false);



	}
	private void Update()
	{
		if (Input.GetButtonDown("Jump"))
		{
			anim.SetBool("Jump", true);
		}
		if (Input.GetButtonUp("Jump"))
		{
			anim.SetBool("Jump", false);
		}
	}


}
