﻿using UnityEngine.Audio;
using UnityEngine;
using System;
using System.Collections;

public class AudioManager : MonoBehaviour
{
	public Sounds[] Raffles_Sounds;
	public Sounds[] Farquhar_Sounds;
	AudioSource audioSource;

    void Awake()
    {
		audioSource = GetComponent<AudioSource>();

		foreach (Sounds s in Raffles_Sounds)
		{
			s.source = audioSource;
			s.source.volume = s.volume;
		}
		foreach (Sounds e in Farquhar_Sounds)
		{
			e.source = audioSource;
			e.source.volume = e.volume;
		}
	}
//public void Play (string name)
	//{
	//	Sounds s = Array.Find(Raffles_Sounds, Sounds => Sounds.name == name);
		//s.source.Play();
	//}
	public void RafflesRandomAudio()
	{
		Sounds s = Raffles_Sounds[UnityEngine.Random.Range(0, Raffles_Sounds.Length)];

		s.source.clip = s.clip;
		s.source.Play();
	}
	public void FarquharRandomAudio()
	{
		Sounds e = Farquhar_Sounds[UnityEngine.Random.Range(0, Farquhar_Sounds.Length)];

		e.source.clip = e.clip;
		e.source.Play();
	}
}
